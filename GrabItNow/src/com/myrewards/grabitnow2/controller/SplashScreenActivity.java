package com.myrewards.grabitnow2.controller;

import org.jsoup.Jsoup;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.TextView;

import com.myrewards.grabitnow2.service.AlertsListener;
import com.myrewards.grabitnow2.service.CustomDialog;
import com.myrewards.grabitnow2.utils.Utility;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint({ "HandlerLeak", "ShowToast" })
public class SplashScreenActivity<TextProgressBar> extends Activity implements
		AnimationListener,AlertsListener {

	// stopping splash screen starting home activity.
	private static final int STOPSPLASH = 0;
	// time duration in millisecond for which your splash screen should visible to
	// user. here i have taken half second
	private static final long SPLASHTIME = 2500;
	
	final private static int UPDATE_AVAILABLE = 1;
	final private static int NO_INTERNET = 2;
	
	//Alert Dialog Members variables 
	private TextView alertTitleTV, alertMessageTV;
	private Button yesBtn, noBtn;
	private String alertTitleStr, alertMsgStr, alertYesStr, alertNoStr;

	Integer currentAPILevel;
	// handler for splash screen
	private Handler splashHandler = new Handler() {
		
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case STOPSPLASH:
				// Generating and Starting new intent on splash time out
				boolean newVersion = false;
				if (Utility.isOnline((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
					if (!(currentAPILevel > 8)) {
						Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
						startActivity(intent);
						SplashScreenActivity.this.finish();
					} else {
						newVersion = web_update();
						if (newVersion == true) {
							alertTitleStr = getResources().getString(R.string.update_available_text);
							alertMsgStr = getResources().getString(R.string.update_version_message);
							alertYesStr = getResources().getString(R.string.yes_one);
							alertNoStr = getResources().getString(R.string.no_one);
						//	showDialog(UPDATE_AVAILABLE);
							
							CustomDialog.getInstance().showCustomDialog(SplashScreenActivity.this, alertTitleStr, alertMsgStr, alertYesStr, alertNoStr, UPDATE_AVAILABLE);
							
						} else {
							Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
							startActivity(intent);
							SplashScreenActivity.this.finish();
						}
					}
				} else {
					alertTitleStr = getResources().getString(R.string.alert_title_no_network_ava);
					alertMsgStr = getResources().getString(R.string.no_network_avail);
					alertYesStr = getResources().getString(R.string.ok_one);
					alertNoStr = "EMPTY";
					
					CustomDialog.getInstance().showCustomDialog(SplashScreenActivity.this, alertTitleStr, alertMsgStr, alertYesStr, alertNoStr, NO_INTERNET);
					
					//showDialog(NO_INTERNET);
				}
				break;
			}
			super.handleMessage(msg);
		}

		private boolean web_update() {
			try {
				String package_name = getPackageName();
				String curVersion = getApplicationContext().getPackageManager()
						.getPackageInfo(package_name, 0).versionName;
				String newVersion = curVersion;
				newVersion = Jsoup
						.connect(
								"https://play.google.com/store/apps/details?id="
										+ package_name + "&hl=en")
						.timeout(30000)
						.userAgent(
								"Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
						.referrer("http://www.google.com").get()
						.select("div[itemprop=softwareVersion]").first()
						.ownText();
				return (value(curVersion) < value(newVersion)) ? true : false;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		private long value(String string) {
			string = string.trim();
			if (string.contains(".")) {
				final int index = string.lastIndexOf(".");
				return value(string.substring(0, index)) * 100
						+ value(string.substring(index + 1));
			} else {
				return Long.valueOf(string);
			}
		}
	};

	@SuppressLint("HandlerLeak")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.splashscreen);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("Hari-->", e);
			}
		}

		try {
			currentAPILevel = Integer.valueOf(android.os.Build.VERSION.SDK);
			if (currentAPILevel >= 11) {
				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy); 
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("Hari-->", e);
			}
		}

		try {
			Message msg = new Message();
			msg.what = STOPSPLASH;
			splashHandler.sendMessageDelayed(msg, SPLASHTIME);
		} catch (NoSuchFieldError e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("Hari-->", e);
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}
	
	
	@Override
	public void onAnimationEnd(Animation arg0) {
		System.out.println("This is under animation starts ");
	}
	@Override
	public void onAnimationRepeat(Animation arg0) {
	}
	@Override
	public void onAnimationStart(Animation arg0) {
	}

	@Override
	public void onDialogPressed(int id, String buttonPressed) 
		{

		if (id == UPDATE_AVAILABLE) 
		{
			if (buttonPressed
					.equals(getResources().getString(R.string.yes_one))) 
			{
				startActivity(new Intent(SplashScreenActivity.this,
						AppPushNotificationActivity.class));
				SplashScreenActivity.this.finish();
			} 
			else {
				startActivity(new Intent(SplashScreenActivity.this,
						LoginActivity.class));
				SplashScreenActivity.this.finish();
			}
		} else if (id == NO_INTERNET) {
			SplashScreenActivity.this.finish();
		}
	}
}