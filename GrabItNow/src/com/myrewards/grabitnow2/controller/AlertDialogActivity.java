package com.myrewards.grabitnow2.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.myrewards.grabitnow2.utils.Utility;

public class AlertDialogActivity extends Activity {
	private TextView alertTitleTV, alertMessageTV;
	private Button yesBtn, noBtn;
	private String alertTitleStr, alertMsgStr, alertYesStr, alertNoStr;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			Settings.System.putInt(getContentResolver(), Settings.System.SOUND_EFFECTS_ENABLED, 1);
			displayAlert();
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	private void displayAlert() {
		alertTitleStr = getResources().getString(R.string.my_parking_timer_dialog_title);
		alertMsgStr = getResources().getString(R.string.my_parking_expire_custom_dialog);
		alertYesStr = getResources().getString(R.string.ok_one);
		alertNoStr = null;
		showDialog(1);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			AlertDialog alertDialog = null;
			LayoutInflater liYes = LayoutInflater.from(this);
			View callAddressView = liYes.inflate(R.layout.alert_all_in_one_dialog, null);
			AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
			adbrok.setCancelable(false);
			adbrok.setView(callAddressView);
			alertDialog = adbrok.create();
			alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
			alertDialog.show();
			return alertDialog;
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->", e);
			}
			return null;
		}
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {

		try {
			final AlertDialog hariAlert = (AlertDialog) dialog;
			alertTitleTV = (TextView) hariAlert.findViewById(R.id.alertYNTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText(""+alertTitleStr);
			
			alertMessageTV = (TextView) hariAlert.findViewById(R.id.alertYNMessageTVID);
			alertMessageTV.setTypeface(Utility.font_reg);
			alertMessageTV.setText(""+alertMsgStr);
			
			yesBtn = (Button) hariAlert.findViewById(R.id.alertYESBtnID);
			yesBtn.setTypeface(Utility.font_bold);
			yesBtn.setText(""+alertYesStr);
			
			noBtn = (Button) hariAlert.findViewById(R.id.alertNOBtnID);
			if (alertNoStr == null) {
				noBtn.setVisibility(View.GONE);
			} else {
				noBtn.setTypeface(Utility.font_bold);
				noBtn.setText(""+alertNoStr);
			}
			
			hariAlert.setCancelable(false);
			
			yesBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					try {
						NewParkingTimeActivity.ReminderSet = false;
						NewParkingTimeActivity.setTime = 0;
						NewParkingTimeActivity.setTimeBtn.setText(getResources().getString(R.string.set_time));
						NewParkingTimeActivity.mAlarmApplication.stopTimer();
					} catch (Exception e) {
						if (e != null) {
							Log.w("Hari-->DEBUG", e);
						}
					}
					AlertDialogActivity.this.finish();
					hariAlert.dismiss();
				}
			});
			
			noBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					hariAlert.dismiss();
				}
			});
		} catch (Exception e) {
			if (null != e) {
				Log.w("HARI-->", e);
			}
		}
	}
}