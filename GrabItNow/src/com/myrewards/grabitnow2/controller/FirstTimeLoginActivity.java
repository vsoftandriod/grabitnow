package com.myrewards.grabitnow2.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.grabitnow2.service.GrabItNowService;
import com.myrewards.grabitnow2.service.ServiceListener;
import com.myrewards.grabitnow2.utils.Utility;
import com.myrewards.grabitnow2.xml.FirsttimeLoginParser;

/**
 * 
 * @author HARI
 * 
 *This class is used to register when user is login for first time
 */

@SuppressLint("ShowToast")
public class FirstTimeLoginActivity extends Activity implements
		android.view.View.OnClickListener, OnItemSelectedListener,
		ServiceListener, OnLongClickListener {
	private EditText firstNameEt, lastNameEt, passwdEt, confpasswdEt, emailEt,
			referalNumberET, tandcFLED;
	protected CheckBox chBox1, chBox2;
	private Button submitBtn1, closebtn;
	private Spinner stateSP2, countrySP1;
	public Animation movement5;
	public LinearLayout firstTimeLoginLL;
	final private static int FIELDS_ERROR = 1;
	final private static int SUCCESS_REG = 2;
	final private static int REQUIRED_PASSWORD = 3;
	final private static int DIALOG_SEND_INVALID_EMAIL = 4;
	final private static int FAILED_REG = 5;
	final private static int SELECT_COUNTRY = 6;
	final private static int SELECT_AGREE = 7;
	
	//Alert Dialog Members variables 
	private TextView alertTitleTV, alertMessageTV;
	private Button yesBtn, noBtn;
	private String alertTitleStr, alertMsgStr, alertYesStr, alertNoStr;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first_time_login);
		firstTimeLoginLL = (LinearLayout) findViewById(R.id.firstTimeLoginLLID);

		firstNameEt = (EditText) findViewById(R.id.firstNameETID);
		firstNameEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		firstNameEt.setTypeface(Utility.font_reg);
		lastNameEt = (EditText) findViewById(R.id.lastNameETID);
		lastNameEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		lastNameEt.setTypeface(Utility.font_reg);
		passwdEt = (EditText) findViewById(R.id.passwdETID);
		passwdEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		passwdEt.setTypeface(Utility.font_reg);
		confpasswdEt = (EditText) findViewById(R.id.confPasswdETID);
		confpasswdEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		confpasswdEt.setTypeface(Utility.font_reg);
		emailEt = (EditText) findViewById(R.id.emailETID);
		emailEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		emailEt.setTypeface(Utility.font_reg);
		referalNumberET = (EditText) findViewById(R.id.referalNumberETID);
		referalNumberET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		referalNumberET.setTypeface(Utility.font_reg);
		tandcFLED = (EditText) findViewById(R.id.tandcFLEDID);
		tandcFLED.getLayoutParams().height = (int) (Utility.screenHeight / 4);
		tandcFLED.setTypeface(Utility.font_reg);

		firstNameEt.setOnLongClickListener(this);
		lastNameEt.setOnLongClickListener(this);
		passwdEt.setOnLongClickListener(this);
		confpasswdEt.setOnLongClickListener(this);
		emailEt.setOnLongClickListener(this);

		movement5 = AnimationUtils.loadAnimation(this, R.anim.animation_first_time_login);

		// You can now apply the animation to a view
		firstTimeLoginLL.startAnimation(movement5);

		submitBtn1 = (Button) findViewById(R.id.submitBtnID);
		submitBtn1.setTypeface(Utility.font_bold);
		submitBtn1.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		submitBtn1.setOnClickListener(this);

		closebtn = (Button) findViewById(R.id.closeftlBtnID);
		closebtn.setOnClickListener(this);

		chBox1 = (CheckBox) findViewById(R.id.checKBox_id1);
		chBox2 = (CheckBox) findViewById(R.id.checKBox_id2);

		// select country
		countrySP1 = (Spinner) findViewById(R.id.countrySpinnerID);
		countrySP1.setOnItemSelectedListener(this);

		// select state
		stateSP2 = (Spinner) findViewById(R.id.stateSpinnerID);
		stateSP2.setOnItemSelectedListener(this);

		stateSP2.setVisibility(View.GONE);
		ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(
				this, R.array.category_countries,
				android.R.layout.simple_spinner_item);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		countrySP1.setAdapter(adapter1);

		countrySP1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				ArrayAdapter<CharSequence> adapter2 = null;
				if (arg2 == 0) {
					adapter2 = ArrayAdapter.createFromResource(FirstTimeLoginActivity.this, R.array.selectstatearray,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 1) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_australia,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}

				} else if (arg2 == 2) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_hongkong,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 3) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_india,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 4) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_newzealand,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 5) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_philippines,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 6) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_singapore,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.submitBtnID) {
			String stateSPITEMS[][] = {
					{ "" },
					{ "", "ACT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA" },
					{ "HK" },
					{ "", "AP", "DL", "GJ", "HR", "KA", "MH", "TN", "UP", "WB" },
					{ "", "AUK", "BOP", "CAN", "FIL", "GIS", "HKB", "MBH",
							"MWT", "NAB", "NTL", "OTA", "STL", "TKI", "TMR",
							"TSM", "WGI", "WGN", "WKO", "WPP", "WTC" },
					{ "", "LUZ", "MIN", "NCR", "VIS" },
					{ "", "CS", "NES", "NWS", "SES", "SWS" } };

			String mFirstName = firstNameEt.getText().toString();
			String mLastName = lastNameEt.getText().toString();
			String mPasswd = passwdEt.getText().toString();
			String mConPasswd = confpasswdEt.getText().toString();
			String mEmail = emailEt.getText().toString();
			String mCountrySP = countrySP1.getSelectedItem().toString();
			String mStateSP = stateSP2.getSelectedItem().toString();

			int countrySPPOS = countrySP1.getSelectedItemPosition();
			int stateSPPOS = stateSP2.getSelectedItemPosition();

			int newslatter;
			if (chBox2.isChecked()) {
				newslatter = 1;
			} else {
				newslatter = 0;
			}

			if (mFirstName.length() > 0 && mLastName.length() > 0
					&& mPasswd.length() > 0 && mEmail.length() > 0
					&& mCountrySP.length() > 0 && mStateSP.length() > 0
					&& FirsttimeLoginParser.first_login_id != 0) {
				if (mPasswd.equals(mConPasswd)) {
					if (isEmailValid(mEmail)) {
						if (!(mCountrySP.equals("Select Country"))
								&& !(mStateSP.equals("Select State"))) {
							if (chBox1.isChecked() == true) {
								if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
									GrabItNowService.getGrabItNowService()
									.sendFirstTimeDetails(
											FirstTimeLoginActivity.this,
											mFirstName,
											mLastName,
											mPasswd,
											mEmail,
											mCountrySP,
											stateSPITEMS[countrySPPOS][stateSPPOS],
											newslatter,
											FirsttimeLoginParser.first_login_id);
								} else {
									// The Custom Toast Layout Imported here
									LayoutInflater inflater = getLayoutInflater();
									View layout = inflater.inflate(R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));
									 
									// The actual toast generated here.
									Toast toast = new Toast(getApplicationContext());
									toast.setDuration(Toast.LENGTH_LONG);
									toast.setView(layout);
									toast.show();
									startActivity(new Intent(FirstTimeLoginActivity.this, LoginActivity.class));
									FirstTimeLoginActivity.this.finish();
								}
							} else {
								alertTitleStr = getResources().getString(R.string.alert_title_requd);
								alertMsgStr = getResources().getString(R.string.alert_title_requd_i_agre);
								alertYesStr = getResources().getString(R.string.ok_one);
								alertNoStr = null;
								showDialog(SELECT_AGREE);
							}
						} else {
							alertTitleStr = getResources().getString(R.string.alert_title_requd);
							alertMsgStr = getResources().getString(R.string.alert_title_requd_c_s);
							alertYesStr = getResources().getString(R.string.ok_one);
							alertNoStr = null;
							showDialog(SELECT_COUNTRY);
						}
					} else {
						alertTitleStr = getResources().getString(R.string.warg_alert);
						alertMsgStr = getResources().getString(R.string.field_error_invalid_email);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = null;
						showDialog(DIALOG_SEND_INVALID_EMAIL);
					}
				} else {
					alertTitleStr = getResources().getString(R.string.warg_alert);
					alertMsgStr = getResources().getString(R.string.password_fields_error);
					alertYesStr = getResources().getString(R.string.ok_one);
					alertNoStr = null;
					showDialog(REQUIRED_PASSWORD);
				}
			} else {
				// custom alert for mandatory fields.....HARI
				alertTitleStr = getResources().getString(R.string.warg_alert);
				alertMsgStr = getResources().getString(R.string.all_fields_error);
				alertYesStr = getResources().getString(R.string.ok_one);
				alertNoStr = null;
				showDialog(FIELDS_ERROR);
			}
		} else if (v.getId() == R.id.closeftlBtnID) {
			startActivity(new Intent(FirstTimeLoginActivity.this, LoginActivity.class));
			FirstTimeLoginActivity.this.finish();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent in = new Intent(getApplicationContext(),	LoginActivity.class);
			startActivity(in);
			FirstTimeLoginActivity.this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			AlertDialog alertDialog = null;
			LayoutInflater liYes = LayoutInflater.from(this);
			View callAddressView = liYes.inflate(R.layout.alert_all_in_one_dialog, null);
			AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
			adbrok.setView(callAddressView);
			alertDialog = adbrok.create();
			alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
			alertDialog.show();
			return alertDialog;
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->", e);
			}
			return null;
		}
	}

	@Override
	protected void onPrepareDialog(final int id, Dialog dialog) {
		try {
			final AlertDialog hariAlert = (AlertDialog) dialog;
			alertTitleTV = (TextView) hariAlert.findViewById(R.id.alertYNTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText(""+alertTitleStr);
			
			alertMessageTV = (TextView) hariAlert.findViewById(R.id.alertYNMessageTVID);
			alertMessageTV.setTypeface(Utility.font_reg);
			alertMessageTV.setText(""+alertMsgStr);
			
			yesBtn = (Button) hariAlert.findViewById(R.id.alertYESBtnID);
			yesBtn.setTypeface(Utility.font_bold);
			yesBtn.setText(""+alertYesStr);
			
			noBtn = (Button) hariAlert.findViewById(R.id.alertNOBtnID);
			if (alertNoStr == null) {
				noBtn.setVisibility(View.GONE);
			} else {
				noBtn.setTypeface(Utility.font_bold);
				noBtn.setText(""+alertNoStr);
			}
			
			if (id == SUCCESS_REG) {
				hariAlert.setCancelable(false);
			}
			
			yesBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (id == SUCCESS_REG) {
						startActivity(new Intent(FirstTimeLoginActivity.this, LoginActivity.class));
						FirstTimeLoginActivity.this.finish();
					}
					hariAlert.dismiss();
				}
			});
			
			noBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					hariAlert.dismiss();
				}
			});
		} catch (Exception e) {
			if (null != e) {
				Log.w("HARI-->", e);
			}
		}
	}
	
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType == 15) {
				if (response == null) {
					alertTitleStr = getResources().getString(R.string.alert_title_failed_sent);
					alertMsgStr = getResources().getString(R.string.alert_title_failed_sent_text);
					alertYesStr = getResources().getString(R.string.ok_one);
					alertNoStr = null;
					showDialog(FAILED_REG);
				}
				if (response != null) {
					if (response instanceof String) {
						alertTitleStr = getResources().getString(R.string.success_title);
						alertMsgStr = getResources().getString(R.string.success_first_login);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = null;
						showDialog(SUCCESS_REG);
					}
				}
			}
		} catch (Exception e) {
			if (null != e) {
				Log.v("Hari---> sendFrnd OnserviceComplete", e.getMessage());
			}
		}
	}
	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = true;
		if (v.getId() == R.id.firstNameETID) {
			if (firstNameEt.getText().length() > 25) {
				returnValue = true;
			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.lastNameETID) {
			if (lastNameEt.getText().length() > 25) {
				returnValue = true;
			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.passwdETID) {
			if (passwdEt.getText().length() > 25) {
				returnValue = true;
			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.confPasswdETID) {
			if (confpasswdEt.getText().length() > 25) {
				returnValue = true;
			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.emailETID) {
			if (emailEt.getText().length() > 60) {
				returnValue = true;
			} else {
				returnValue = false;
			}
		}
		return returnValue;
	}
}
