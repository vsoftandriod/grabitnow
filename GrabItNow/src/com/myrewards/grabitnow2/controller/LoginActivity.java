package com.myrewards.grabitnow2.controller;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.grabitnow2.imagecache.WebImageCache;
import com.myrewards.grabitnow2.model.LoginDetails;
import com.myrewards.grabitnow2.model.User;
import com.myrewards.grabitnow2.service.AlertsListener;
import com.myrewards.grabitnow2.service.CustomDialog;
import com.myrewards.grabitnow2.service.GrabItNowService;
import com.myrewards.grabitnow2.service.ServiceListener;
import com.myrewards.grabitnow2.timer.CrittercismAndroid;
import com.myrewards.grabitnow2.utils.AnimeUtils;
import com.myrewards.grabitnow2.utils.ApplicationConstants;
import com.myrewards.grabitnow2.utils.DatabaseHelper;
import com.myrewards.grabitnow2.utils.Utility;
import com.myrewards.grabitnow2.xml.FirsttimeLoginParser;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.plattysoft.leonids.Particle;
import com.plattysoft.leonids.ParticleSystem;

@SuppressLint({ "SimpleDateFormat", "ShowToast" })
public class LoginActivity extends Activity implements OnClickListener,
		ServiceListener, AnimationListener, OnLongClickListener,AlertsListener {
	public static EditText etUserId;
	public static EditText etPasswd;
	RelativeLayout parentRL;
	public static EditText subDomainURL_et;
	RelativeLayout parentSubLayout;
	ArrayAdapter<String> adapter;
	Button btnLogin, firstTimeLoginBtn;
	private InputMethodManager imm;
	DatabaseHelper dbHelper;
	ImageView splashIV;
	public static EditText msNoet;
	public static EditText webAdds;
	// public static AlertDialog dialogDetails2;
	SimpleDateFormat formatter;
	Dialog dialog;

	public Animation movement5;
	final private static int FIRST_TIME_LOGIN_BUTTON = 1;
	final private static int NO_NETWORK_CON = 2;
	final private static int LOGIN_FAILED = 3;
	final private static int BOTH_EMPTY = 4;

	// Membership validation
	public TextView tv12;
	LoginDetails loginDetails;
	RelativeLayout loadingPanel;
	Bundle bundle = null;
	boolean finishState;
	
	private TextView alertTitleTV, alertMessageTV;
	private Button yesBtn, noBtn;
	private String alertTitleStr, alertMsgStr, alertYesStr, alertNoStr;

	public void deleteCmpleteCacheData() {
		try{
			deleteCache(this);
			WebImageCache cache=new WebImageCache(this);
			cache.clear();
			}
			catch(Exception e){
				if(e!=null) {
					e.printStackTrace();
				}
			}
	}
	public static void deleteCache(Context context) {
	    try {
	        File dir = context.getCacheDir();
	        if (dir != null && dir.isDirectory()) {
	            deleteDir(dir);
	        }
	    } catch (Exception e) {}
	}
	public static boolean deleteDir(File dir) {
	    if (dir != null && dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i = 0; i < children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }
	    return dir.delete();
	}
	
	
	//this is for star animation
	
	int[] resourceDrawables=
		{
	R.drawable.star_one_one,R.drawable.star_two_one,R.drawable.star_three_one,R.drawable.star_four_one
	,R.drawable.star_one_two,R.drawable.star_two_two,R.drawable.star_three_two,R.drawable.star_four_two	
	,R.drawable.star_one_three,R.drawable.star_two_three,R.drawable.star_three_three,R.drawable.star_four_one
	,R.drawable.star_one_four,R.drawable.star_two_four,R.drawable.star_three_one,R.drawable.star_four_two
		};
	
	
	
	ParticleSystem partSystemOB;
	
	private boolean keypadOpened=false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);

		
		
		
		deleteCmpleteCacheData();
		
		// Push Notifications Adding...................
		// Parse.initialize(this, YOUR_APPLICATION_ID, YOUR_CLIENT_KEY);
		Parse.initialize(this, "kDL13Y2lOsE3AwzEVAOWd7t5uqKJWfFXmiGoS8bf", "VHEFQBRkugWTKGy6r1JxSfBMXEYtlbBttOvfcGFd");

		PushService.setDefaultPushCallback(this, AppPushNotificationActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
		ParseAnalytics.trackAppOpened(getIntent());

		setDimensions();
		ApplicationConstants.USERNAME_PASSWORD_FIELDS = getResources()
				.getString(R.string.uname_paswd_app);
		ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL = getResources()
				.getString(R.string.invalid_url_estb);
		ApplicationConstants.UNABLETOESTABLISHCONNECTION = getResources()
				.getString(R.string.unable_to_estb);
		dbHelper = new DatabaseHelper(this);
		etUserId = (EditText) findViewById(R.id.usernameETID);

		etUserId.setTypeface(Utility.font_reg);
		
		parentSubLayout = (RelativeLayout) findViewById(R.id.loginRLID);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(Utility.screenWidth / 18, Utility.screenWidth / 18, Utility.screenWidth / 18, Utility.screenWidth / 18);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		parentSubLayout.setLayoutParams(params);

		etUserId.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		etPasswd = (EditText) findViewById(R.id.passwordETID);
		etPasswd.setTypeface(Utility.font_reg);

		etPasswd.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		subDomainURL_et = (EditText) findViewById(R.id.subDomainETID);
		subDomainURL_et.setTypeface(Utility.font_reg);

		subDomainURL_et.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		btnLogin = (Button) findViewById(R.id.loginBtnID);
		btnLogin.setTypeface(Utility.font_bold);
		btnLogin.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		btnLogin.setOnClickListener(this);

	
		/*etUserId.setText("sasi");
		etPasswd.setText("sasi123");
		subDomainURL_et.setText("www.myrewards.com.au");*/
		
		
		etUserId.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		etPasswd.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		
		
		subDomainURL_et.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		etUserId.setOnLongClickListener(this);
		etPasswd.setOnLongClickListener(this);
		subDomainURL_et.setOnLongClickListener(this);
		
		GPSTracker mGPS = new GPSTracker(this);
		if (mGPS.canGetLocation) {
			double mLat = mGPS.getLatitude();
			double mLong = mGPS.getLongitude();
			Utility.mLat = mLat;
			Utility.mLng = mLong;

			Log.v("mLatmLatmLat", "=" + mLat);
			Log.v("mLongmLongmLongmLong", "=" + mLong);

		} else {
			// can't get the location
		}
		splashIV = (ImageView) findViewById(R.id.splashIVID);
		loadingPanel = (RelativeLayout) findViewById(R.id.loadingPanelNew);
		splashIV.setVisibility(ImageView.GONE);

		try {
			loginDetails = dbHelper.getLoginDetails();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->DEBUG", e);
			}
		}
		Date loginDate = null;
		Date currentDate = null;
		// Date Format: MMM DD, YYYY hh:mm:ss ex: Mar 11, 2013 10:03:08 PM
		if (loginDetails != null) {
			try {
				formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				try {
					loginDate = formatter.parse(loginDetails.getLoginDate());
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("HARI-->DEBUG", e);
					}
				}
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Calendar cal = Calendar.getInstance();
				String currentDateString = dateFormat.format(cal.getTime());
				try {
					currentDate = formatter.parse(currentDateString);
				} catch (ParseException e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("HARI-->DEBUG", e);
					}
				}

				long diff = currentDate.getTime() - loginDate.getTime();
				long days = diff / (1000 * 60 * 60 * 24);
				if (days < 7) {
					splashIV.setVisibility(ImageView.GONE);
					if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						GrabItNowService.getGrabItNowService().sendLoginRequest(this, loginDetails.getUsername(),loginDetails.getPassword(),loginDetails.getSubDomain());
					} else {
						alertTitleStr = getResources().getString(R.string.alert_title_no_network_ava);
						alertMsgStr = getResources().getString(R.string.no_network_avail);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = "EMPTY";
						//showDialog(NO_NETWORK_CON);
						CustomDialog.getInstance().showCustomDialog(LoginActivity.this, alertTitleStr, alertMsgStr, alertYesStr, alertNoStr, NO_NETWORK_CON);
					}
				} else {
					try {
					     dbHelper.deleteLoginDetails();
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.v("Hari-->", e.getMessage());
					   }
					}
					loginDetails=null;
					splashIV.setVisibility(ImageView.GONE);
					loadingPanel.setVisibility(View.GONE);
					// Animation //
					parentSubLayout.setVisibility(View.VISIBLE);
					// movement5 =
					
					movement5 = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.accelerate_login_page);
					movement5.reset();
					movement5.setFillAfter(true);
					movement5.setAnimationListener(this);
					parentSubLayout.startAnimation(movement5);
					
					startStarEmitAnimation();
					
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->DEBUG", e);
				}
			}
		} else {
			try {
				splashIV.setVisibility(ImageView.GONE);
				loadingPanel.setVisibility(View.GONE);

				parentSubLayout.setVisibility(View.VISIBLE);
				// movement5 = AnimationUtils.loadAnimation(this,R.anim.animation_test5_forlogin);
				movement5 = AnimationUtils.loadAnimation(this, R.anim.accelerate_login_page);
				movement5.reset();
				movement5.setFillAfter(true);
				movement5.setAnimationListener(this);
				parentSubLayout.startAnimation(movement5);
				
				startStarEmitAnimation();
				
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->DEBUG", e);
				}
			}
		}

		firstTimeLoginBtn = (Button) findViewById(R.id.firstTimeloginBtnID);
		firstTimeLoginBtn.setTypeface(Utility.font_bold);
		firstTimeLoginBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		firstTimeLoginBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				showDialog(FIRST_TIME_LOGIN_BUTTON);
			}
		});

		/*try {
			Drawable d;
			d = AnimeUtils.loadDrawableFromResource(getResources(),	R.drawable.sample1);
			((ImageView) findViewById(R.id.imageView1)).setImageDrawable(d);

			String path = Environment.getExternalStorageDirectory()
					+ "/sample1.gif";
			d = AnimeUtils.loadDrawableFromFile(getResources(), path);
		} catch (OutOfMemoryError e) {
			Toast.makeText(this, "Server Busy !", 2000).show();
		}*/
		
		
		//startStarEmitAnimation();
		//changeStarAnimation();
		
		//this is for star animation
		
		
		
		
		//stopStarAnimation();
		//changeStarAnimation();
		//startStarEmitAnimation();
		
		//to listen the keyboard
		
		final View activityRootView = findViewById(R.id.rootViewID);
		activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
		  @Override
		  public void onGlobalLayout() {
			  int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
		        if (heightDiff > 100) 
		        { // if more than 100 pixels, its probably a keyboard...
		          
		          
		        	changeStarAnimation();
		        	
		        	//Toast.makeText(LoginActivity.this, "keyboard visible", Toast.LENGTH_SHORT).show();
		        }
		        else
		        {
		        	
		        	reStartStarAnimation();
		        	
		        	
		        //	Toast.makeText(LoginActivity.this, "keyboard hidden", Toast.LENGTH_SHORT).show();
		        }
		  }
		});
		
	}

	private void startStarEmitAnimation() {
		
		ImageView iv=(ImageView)findViewById(R.id.imageView1);
		
		float starX=(float)((Utility.screenWidth)/2.50);
		float starY=(float)((Utility.screenHeight)/3.0);
		
		new Particle(starX,starY);
		
		partSystemOB=new ParticleSystem(this, 5,resourceDrawables, 3000,(int)(Utility.screenWidth/12.5),(int)(Utility.screenHeight/20));
		
		partSystemOB.setSpeedRange(0.005f, 0.25f)
		.emit(iv,5);
		
	}
	private void stopStarAnimation()
	{
		if(partSystemOB!=null)
		{
		partSystemOB.stopShambhiAnimation();
		}
	}
	private void reStartStarAnimation()
	
	{
		
		float starX=(float)((Utility.screenWidth)/2.50);
		float starY=(float)((Utility.screenHeight)/3.0);
		
		Particle.mInitialX=starX;
		Particle.mInitialY=starY;
		
/*ImageView iv=(ImageView)findViewById(R.id.imageView1);
		
		float starX=50;
		float starY=50;
		
		new Particle(starX,starY);
		
		partSystemOB=new ParticleSystem(this, 5,resourceDrawables, 3000,(int)(Utility.screenWidth/12.5),(int)(Utility.screenHeight/20));
		
		partSystemOB.setSpeedRange(0.005f, 0.25f)
		.emit(iv,10);*/
	}
	private void changeStarAnimation()
	{
		
		float starX=(float)((Utility.screenWidth)/2.50);
		float starY=(float)((Utility.screenHeight)/6);
		
		Particle.mInitialX=starX;
		Particle.mInitialY=starY;
		
		
		//Particle.mInitialX=50;
		//Particle.mInitialY=50;	
	}
	
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		try {
			// start/stop animation
			AnimeUtils.startViewAnimation(findViewById(R.id.imageView1), hasFocus);
			AnimeUtils.startViewAnimation(findViewById(R.id.loginRLID),	hasFocus);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		
			if (id == FIRST_TIME_LOGIN_BUTTON) {
				AlertDialog dialogDetails = null;
				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater.inflate(R.layout.alert_first_time_login_membership, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails.show();
				return dialogDetails;
			
		} 
		
			return null;
		
	}

	@Override
	protected void onPrepareDialog(final int id, Dialog dialog1) {
		dialog = dialog1;
		if (id == FIRST_TIME_LOGIN_BUTTON) {
			final AlertDialog alertDialog1 = (AlertDialog) dialog;
			alertTitleTV = (TextView) alertDialog1.findViewById(R.id.firstLoginTitleTVID);
			alertMessageTV = (TextView) alertDialog1.findViewById(R.id.alertMsgTVID);

			alertTitleTV.setTypeface(Utility.font_bold);
			alertMessageTV.setTypeface(Utility.font_reg);

			yesBtn = (Button) alertDialog1.findViewById(R.id.submitBtn);
			yesBtn.setTypeface(Utility.font_bold);
			
			noBtn = (Button) alertDialog1.findViewById(R.id.btn_cancel);
			noBtn.setTypeface(Utility.font_bold);
			
			msNoet = (EditText) alertDialog1.findViewById(R.id.membershipCardETID);
			msNoet.setTypeface(Utility.font_reg);
			
			webAdds = (EditText) alertDialog1.findViewById(R.id.webAddressETID);
			webAdds.setTypeface(Utility.font_reg);

			msNoet.setOnLongClickListener(this);
			webAdds.setOnClickListener(this);

			yesBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					String membership = msNoet.getText().toString();
					String webAddress = webAdds.getText().toString();
					if ((membership != null && membership.trim().length() >0) && (webAddress != null && webAddress.trim().length() >0)) {
						if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							GrabItNowService.getGrabItNowService().sendFirstTimeLoginDetails(LoginActivity.this, membership, webAddress);
						} else {
							alertTitleStr = getResources().getString(R.string.alert_title_no_network_ava);
							alertMsgStr = getResources().getString(R.string.no_network_avail);
							alertYesStr = getResources().getString(R.string.ok_one);
							alertNoStr = "EMPTY";
							CustomDialog.getInstance().showCustomDialog(LoginActivity.this, alertTitleStr, alertMsgStr, alertYesStr, alertNoStr, NO_NETWORK_CON);
						//	showDialog(NO_NETWORK_CON);
						}
					} else {
						alertTitleStr = getResources().getString(R.string.warg_alert);
						alertMsgStr = getResources().getString(R.string.all_fields_error);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = "EMPTY";
						CustomDialog.getInstance().showCustomDialog(LoginActivity.this, alertTitleStr, alertMsgStr, alertYesStr, alertNoStr, BOTH_EMPTY);
						//showDialog(BOTH_EMPTY);
					}
				}
			});
			
			noBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) 
				{
					alertDialog1.dismiss();
					
					getWindow().setSoftInputMode(
						    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
						);
				}
			});
		 
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.loginBtnID:
			try {
				String userId = etUserId.getText().toString();
				String passwd = etPasswd.getText().toString();
				String subDomainURL = subDomainURL_et.getText().toString();
				
				if ((userId != null && userId.trim().length() > 0) && (passwd != null && passwd.trim().length() > 0)) {
					if (subDomainURL.trim().length() > 0) {
						try {
							issueRequest(userId, passwd, subDomainURL);
						} catch (Exception e) {
							if (e != null) {
								Log.w("HARI-->DEBUG", e);
							}
						}
					} else {
						alertTitleStr = getResources().getString(R.string.warg_alert);
						alertMsgStr = getResources().getString(R.string.all_fields_error);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = "EMPTY";
						CustomDialog.getInstance().showCustomDialog(LoginActivity.this, alertTitleStr, alertMsgStr, alertYesStr, alertNoStr, BOTH_EMPTY);
					//	showDialog(BOTH_EMPTY);
						btnLogin.setEnabled(true);
					}
				} else {
					alertTitleStr = getResources().getString(R.string.warg_alert);
					alertMsgStr = getResources().getString(R.string.all_fields_error);
					alertYesStr = getResources().getString(R.string.ok_one);
					alertNoStr = "EMPTY";
					CustomDialog.getInstance().showCustomDialog(LoginActivity.this, alertTitleStr, alertMsgStr, alertYesStr, alertNoStr, BOTH_EMPTY);
				//	showDialog(BOTH_EMPTY);
					btnLogin.setEnabled(true);
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
			}
			break;
		}
	}

	private void issueRequest(String userId, String password,
			String subDomainURL) {
		try {
			imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(etUserId.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(etPasswd.getWindowToken(), 0);
			System.out.println("password: " + password);
			if (Utility.isOnline((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
				GrabItNowService.getGrabItNowService().sendLoginRequest(this, userId, password, subDomainURL);
				splashIV.setVisibility(ImageView.VISIBLE);
			} else {
				alertTitleStr = getResources().getString(R.string.alert_title_no_network_ava);
				alertMsgStr = getResources().getString(R.string.no_network_avail);
				alertYesStr = getResources().getString(R.string.ok_one);
				alertNoStr = "EMPTY";
				CustomDialog.getInstance().showCustomDialog(LoginActivity.this, alertTitleStr, alertMsgStr, alertYesStr, alertNoStr, NO_NETWORK_CON);
				//showDialog(NO_NETWORK_CON);
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType == 1 || eventType == 2) {
				if (response != null) {
					if (response instanceof String) {
						alertTitleStr = getResources().getString(R.string.alert_title_login_failed);
						alertMsgStr = getResources().getString(R.string.uname_paswd_app);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = "EMPTY";
						CustomDialog.getInstance().showCustomDialog(LoginActivity.this, alertTitleStr, alertMsgStr, alertYesStr, alertNoStr, LOGIN_FAILED);
					//	showDialog(LOGIN_FAILED);
						splashIV.setVisibility(ImageView.GONE);
					} else {
						if (loginDetails == null) {
							try {
								DateFormat dateFormat = new SimpleDateFormat(
										"yyyy/MM/dd HH:mm:ss");
								Calendar cal = Calendar.getInstance();

								dbHelper.addLoginDetails(etUserId.getText()
										.toString(), etPasswd.getText()
										.toString(), subDomainURL_et.getText()
										.toString(), dateFormat.format(cal
										.getTime()));
							} catch (Exception e) {
								if (e != null) {
									e.printStackTrace();
									Log.w("HARI-->DEBUG", e);
								}
							}
							Utility.user_Name = etUserId.getText().toString();
							Utility.user_Password = etPasswd.getText().toString();
							Utility.user_website = subDomainURL_et.getText().toString();
						}
						if (Utility.user_Name == null) {
							Utility.user_Name = dbHelper.getLoginDetails().getUsername();
							Utility.user_Password = dbHelper.getLoginDetails().getPassword();
							Utility.user_website = dbHelper.getLoginDetails().getSubDomain();
						}
						Utility.user = (User) response;

						try {
							if (dbHelper.getLoginDetails() != null) {
								if (dbHelper.getLoginDetails().getUsername() != null) {
									String uName = dbHelper.getLoginDetails().getUsername().toString();
									CrittercismAndroid.SetUsername(uName);
								}
							}
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
								Log.w("Hari--> Login Page", e);
							}
						}

						Intent i = new Intent(LoginActivity.this, SearchListActivity.class);
						i.putExtra("LOGIN_ACTIVITY", "LOGIN_ACTIVITY");
						startActivity(i);
						LoginActivity.this.finish();
					}
				}
			} else if (eventType == 14) {
				if (response != null) {
					if (FirsttimeLoginParser.firsttimeloginresponce.equalsIgnoreCase("TRUE")) {
						Animation shake = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.shake_login);
						msNoet.startAnimation(shake);
						webAdds.startAnimation(shake);
					} else if (FirsttimeLoginParser.firsttimeloginresponce.equalsIgnoreCase("FALSE")) {
						startActivity(new Intent(LoginActivity.this, FirstTimeLoginActivity.class));
						LoginActivity.this.finish();
						dialog.dismiss();
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	private void setDimensions() {
		try {
			Display display = getWindowManager().getDefaultDisplay();
			int screenWidth = display.getWidth();
			int screenHeight = display.getHeight();
			Utility.screenWidth = screenWidth;
			Utility.screenHeight = screenHeight;
			parentRL = (RelativeLayout) findViewById(R.id.loginParentRLID);

			Utility.font_bold = Typeface.createFromAsset(this.getAssets(),"helvetica_bold.ttf");
			Utility.font_reg = Typeface.createFromAsset(this.getAssets(),"helvetica_reg.ttf");
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	protected void onRestart() {
		splashIV.setVisibility(ImageView.GONE);
		super.onRestart();
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub

	}
@Override
public boolean onTouchEvent(MotionEvent event) {
	// TODO Auto-generated method stub
     	return super.onTouchEvent(event);
}
	@Override
	public boolean onLongClick(View v) {
		boolean returnValue;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod(v, stringLength);

		} catch (Exception e) {
			returnValue = false;
		}
		return returnValue;
	}
	
	@Override
	protected void onDestroy() 
	{
		try
		{
		if(partSystemOB!=null)
		{
		partSystemOB.stopShambhiAnimation();
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		super.onDestroy();
	}
	@Override
	public void onDialogPressed(int id, String buttonPressed) 
	{
		if(id==FIRST_TIME_LOGIN_BUTTON)
		{
			
		}
		else if(id==NO_NETWORK_CON)
		{
			loadingPanel.setVisibility(View.GONE);
		}
		else if(id==LOGIN_FAILED)
		{

			if (parentSubLayout.getVisibility() == View.GONE) {
				parentSubLayout.setVisibility(View.VISIBLE);
				loadingPanel.setVisibility(View.GONE);
				try {
					if (dbHelper != null) {
						dbHelper.deleteLoginDetails();
						loginDetails = dbHelper.getLoginDetails();
					}
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("HARI-->DEBUG", e);
					}
				}
			}
		
		
		}
		else if(id==BOTH_EMPTY)
		{
			
		}
	}
	
}
