package com.myrewards.grabitnow2.service;

public interface NetworkListener {
	void onRequestCompleted(String response, String errorString, int eventType);

}
