package com.myrewards.grabitnow2.service;

public interface ServiceListener {
	public void onServiceComplete(Object response, int eventType);
}
